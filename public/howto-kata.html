<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>virtio-fs - shared file system for virtual machines / Kata HowTo</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="navbar">
      <a href="/">Home</a>
    </div>

    <h1>How to use Kata Containers with virtio-fs</h1>
    <h2>Update for Kata Containers 1.7 and later</h2>
    <p>This HowTo is obsolete as of Kata Containers 1.7.
      virtio-fs has been included in Kata Containers and can be enabled as
      described in the <a
      href="https://github.com/kata-containers/documentation/blob/master/how-to/how-to-use-virtio-fs-with-kata.md">official
      Kata Containers documentation</a>.  It is no longer necessary to build
      from virtio-fs repositories since mainline Kata Containers now includes
      virtio-fs.
    </p>

    <h2>Overview</h2>
    <p>
      This document describes how to set up Kata Containers with virtio-fs.
      Container images will be exposed to the sandbox VM using virtio-fs.
    </p>
    <p>
      Use this guide if you wish to test or benchmark Kata Container workloads.
      It is easier to develop and debug virtio-fs standalone without Kata due
      to the smaller number of components involved.  A guide for manually
      running QEMU with virtio-fs is available <a
      href="/howto-qemu.html">here</a>.
    </p>

    <p>
      <a href="https://katacontainers.io/">Kata Containers</a> is an OCI runtime that runs containers inside a virtual
      machine for better isolation. Docker and Kubernetes (CRI-O) can be
      configured to launch Kata Containers instead of their default OCI
      runtimes.
    </p>

    <p>
      Each VM is called a sandbox. In Kubernetes a sandbox can be thought of
      the same thing as a pod.
    </p>

    <p>
      Containers run inside sandboxes. Each sandbox contains an agent process
      that communicates with the Kata runtime on the host. Commands include
      creating new containers, executing processes inside containers, querying
      sandbox information, etc.
    </p>

    <h2>Prerequisites</h2>
    <ul>
        <li>Fedora 29 host</li>
        <li>Docker Engine installed (<tt>dnf install docker</tt>)</li>
    </ul>

    <h2>Components</h2>
    <p>The following components need to be built:</p>
    <ol type="a">
        <li>A guest kernel with virtio-fs support</li>
        <li>A QEMU with virtio-fs support</li>
        <li>The example virtio-fs daemon (virtiofsd)</li>
        <li>Kata Containers with virtio-fs support</li>
        <li>A Kata Containers initramfs with virtio-fs support</li>
    </ol>

    <p>
      The instructions assume that you already have available a Linux host on
      which you can build and run the components.
    </p>

    <h2>The guest kernel</h2>
    <p>Note that an upstream Linux 5.4 kernel or later can be used as long as the DAX feature is not used.</p>
    <p>On the host, download the virtio-fs kernel tree by:</p>
    <pre>
git clone https://gitlab.com/virtio-fs/linux.git
git checkout virtio-fs-dev
    </pre>
    <p>Configure and build this kernel with the following <tt>.config</tt> file:</p>
    <pre>
wget -O .config <a href="https://gitlab.com/virtio-fs/linux/snippets/1846957/raw">https://gitlab.com/virtio-fs/linux/snippets/1846957/raw</a>
make -j$(nproc)</pre>

    <h2>Building QEMU</h2>
    <p>On the host, download and install the virtio-fs QEMU tree by:</p>
    <pre>
git clone https://gitlab.com/virtio-fs/qemu.git
cd qemu
./configure --target-list=x86_64-softmmu
make -j$(nproc)</pre>

and in the same tree, the virtio-fs daemon needs to be built:
<pre>make -j$(nproc) virtiofsd</pre>

    <h2>Building Kata Containers</h2>
    <h3>Setting up environment variables</h3>
    <p>Decide where you want Go to put source code and packages:</p>
    <pre>export GOPATH=$HOME/go</pre>
    <p>Decide where you want to build the sandbox root filesystem:</p>
    <pre>export ROOTFS_DIR=$GOPATH/src/github.com/kata-containers/osbuilder/rootfs-builder/rootfs-ClearLinux</pre>

    <h3>Cloning Kata Containers repositories</h3>
    <p>Kata consists of several components, each with its own git repository. You need them all.</p>
    <pre>
go get -d -u github.com/kata-containers/runtime
go get -d -u github.com/kata-containers/agent
go get -d -u github.com/kata-containers/osbuilder
go get -d -u github.com/kata-containers/proxy
go get -d -u github.com/kata-containers/shim</pre>

    <p>Ensure you are on Kata 1.6.1:</p>
    <pre>
(cd $GOPATH/src/github.com/kata-containers/agent &amp;&amp; git checkout 1.6.1)
(cd $GOPATH/src/github.com/kata-containers/osbuilder &amp;&amp; git checkout 1.6.1)
(cd $GOPATH/src/github.com/kata-containers/proxy &amp;&amp; git checkout 1.6.1)
(cd $GOPATH/src/github.com/kata-containers/shim &amp;&amp; git checkout 1.6.1)</pre>

    <p>Use the virtio-fs repositories for Kata components that require virtio-fs integration:</p>
    <pre>
    cd $GOPATH/src/github.com/kata-containers/runtime
    git remote add virtio-fs https://gitlab.com/virtio-fs/runtime.git
    git fetch virtio-fs
    git checkout virtio-fs/virtio-fs
    cd -
</pre>

    <h3>Building the runtime</h3>
    <p>
      The runtime presents an OCI-compliant runtime interface to Docker and
      CRI-O. This is where sandbox setup happens and containers are
      orchestrated.
    </p>
    <pre>
cd $GOPATH/src/github.com/kata-containers/runtime/
make</pre>

    <h3>Building the proxy</h3>
    <p>The proxy forwards communications between the host and the sandbox.</p>
    <pre>
cd $GOPATH/src/github.com/kata-containers/proxy
make</pre>

    <h3>Building the shim</h3>
    <p>
      The shim is a placeholder process on the host that forwards terminal I/O
      and signals to the actual container process running inside the sandbox.
    </p>
    <pre>
cd $GOPATH/src/github.com/kata-containers/shim
make</pre>

    <h2>Building the Kata Containers initramfs</h2>
    <p>Build the Fedora root file system:</p>
    <pre>
cd $GOPATH/src/github.com/kata-containers/osbuilder/rootfs-builder
./rootfs.sh clearlinux</pre>

    <p>Now build the sandbox initramfs:</p>
    <pre>
cd $GOPATH/src/github.com/kata-containers/osbuilder/initrd-builder
./initrd_builder.sh $ROOTFS_DIR</pre>

    <h2>Configuring Kata</h2>
    <p>The Kata configuration file controls the behavior of all Kata components.</p>
    <pre>
wget -O /etc/kata-containers/configuration.toml <a href="https://gitlab.com/virtio-fs/runtime/snippets/1846963/raw">https://gitlab.com/virtio-fs/runtime/snippets/1846963/raw</a></pre>

    <p>Set the following <tt>[hypervisor.qemu]</tt> variables:</p>
    <ul>
      <li><tt>path</tt> - Path to your QEMU binary (e.g. .../qemu/x86_64-softmmu/qemu-system-x86_64)</li>
      <li><tt>kernel</tt> - Path to your sandbox kernel (e.g. .../kernel/arch/x86/boot/bzImage)</li>
      <li><tt>initrd</tt> - Path to your sandbox initramfs (e.g. .../go/src/github.com/kata-containers/osbuilder/initrd-builder/kata-containers-initrd.img)</li>
      <li><tt>virtio_fs_daemon</tt> - Path to your qemu build virtiofsd binary</li>
    </ul>

    <p>Set the following <tt>[proxy.kata]</tt> variables:</p>
    <ul>
      <li><tt>path</tt> - Path to your kata-proxy binary (e.g. .../go/src/github.com/kata-containers/proxy/kata-proxy)</li>
    </ul>

    <p>Set the following <tt>[shim.kata]</tt> variables:</p>
    <ul>
      <li><tt>path</tt> - Path to your kata-shim binary (e.g. .../go/src/github.com/kata-containers/shim/kata-shim)</li>
    </ul>

    <p>Also set all <tt>enable_debug</tt> variables to true for verbose output.</p>

    <p>Add the Kata OCI runtime to Docker:</p>
    <pre>
# mkdir -p /etc/systemd/system/docker.service.d
# cat &gt;/etc/systemd/system/docker.service.d/kata-containers.conf
[Service]
Type=simple
ExecStart=
ExecStart=/usr/bin/dockerd-current -D \
          --add-runtime oci=/usr/libexec/docker/docker-runc-current \
          --add-runtime kata-runtime=YOUR_GOPATH/src/github.com/kata-containers/runtime/kata-runtime \
          --default-runtime=oci \
          --containerd /run/containerd.sock \
          --userland-proxy-path=/usr/libexec/docker/docker-proxy-current \
          --init-path=/usr/libexec/docker/docker-init-current \
          $OPTIONS \
          $DOCKER_STORAGE_OPTIONS \
          $DOCKER_NETWORK_OPTIONS \
          $ADD_REGISTRY \
          $BLOCK_REGISTRY \
          $INSECURE_REGISTRY \
          $REGISTRIES
^D
# sed -i "s%YOUR_GOPATH%$GOPATH%g" /etc/systemd/system/docker.service.d/kata-containers.conf
# systemctl daemon-reload
# systemctl restart docker</pre>

    <p>
      Ensure there are enough hugepages available on the host for the Kata
      sandbox VM.  The default sandbox RAM size is 2G, so reserve 1024 * 2MB
      hugepages. This is necessary since QEMU is launched with the memory
      options (<tt>share=on</tt>) required by vhost-user.
    </p>
    <pre>sysctl vm.nr_hugepages=1024</pre>

    <h2>Running containers</h2>
    <p>Launch a container:</p>
    <pre>docker run --runtime kata-runtime -it busybox sh</pre>

    <div class="footer">
        <p>This website is published under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International</a> license. The source code is available <a href="https://gitlab.com/virtio-fs/virtio-fs.gitlab.io/">here</a>.</p>
    </div>
  </body>
</html>

